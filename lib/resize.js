"use strict";

const sharp = require('sharp');

module.exports = function(height, width, source, dest) {
    return new Promise((resolve, reject) => {
        sharp(source)
        .resize(width, height)
        .max()
        .toFile(dest, function(err, info){
            if (err) {
                reject(err);
            } else {
                resolve(info);
            }
        });
    });
}
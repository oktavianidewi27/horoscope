"use strict";

const {async} = require("@yesboss/merapi");
const moment = require("moment");
const fs = require("fs");
const azure = require("azure-storage");
const resize = require('./lib/resize');

moment.locale("id");
let now = moment();
let monday = moment().day("senin");

if (monday.isBefore(now))
    monday.add(7, "days");

let date = monday.format("DD-MM");
let originalDirectory = {
    path: "./data/original/",
    affix: date
};
let previewDirectory = {
    path: "./data/preview/",
    affix: `small-${date}`
};

let blobSvc = azure.createBlobService('jemmaimages', "fbqy88ai3J4Wq+3LHYVuAe2huz34kqwqmU0kcwVoZiLZU4rZf69kiwxfGhTYU67XxyncYiH2XfMNExNH1fjSbw==");

function uploadFile({path, affix}) {
    let files = fs.readdirSync(path);

    files.forEach(function (file) {
        if (file.indexOf("jpg") != -1) {
            let splittedName = file.split(/[^a-zA-Z0-9]/);
            let newName =  `${splittedName[0]}-${affix}.${splittedName[splittedName.length - 1]}`;

            let oldFiles = `${path}${file}`;
            let newFiles = `${path}${newName}`;

            blobSvc.createBlockBlobFromLocalFile("jemma-images", newName, oldFiles, function (error, result, response) {
                if (error) {
                    console.log(`Failed to upload ${oldFiles}`);
                }
                else {
                    console.log(`${oldFiles} uploaded successfully as ${newName}`);
                }
            });
        }
    });
}

let resizeFile = async(function* (source, destination) {
    let previewSize = {
        height: 240,
        width: 240
    };
    let files = fs.readdirSync(source.path);
    
    for (let i = 0; i < files.length; i++) {
        try {
            yield resize(previewSize.height, previewSize.width, `${source.path}${files[i]}`, `${destination.path}${files[i]}`);
        } catch (e) {
            console.log(e);
        }
    }
});

let main = async(function* () {
    yield resizeFile(originalDirectory, previewDirectory);
    uploadFile(previewDirectory);
    uploadFile(originalDirectory);
});

main();